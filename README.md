# docker-public

Base Docker Repositories

## php-cli

php:7.3-cli with Composer and Git

## php-nginx

ubuntu:latest with Nginx and PHP-FPM

### Port Mapping

| Port | Https | Authentification |
| --- | --- | --- |
| 80 | Off | Off |
| 81 | On | Off |
| 82 | Off | On |
| 83 | On | On |

### Default Username / Password
`admin:admin`