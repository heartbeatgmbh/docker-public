FROM ubuntu:18.04

RUN apt-get update

RUN apt-get install -y php7.2 php7.2-fpm php7.2-memcached php7.2-mysqli php7.2-json php7.2-curl \
    php7.2-xml php7.2-phar php7.2-intl php7.2-dom php7.2-xmlreader php7.2-ctype \
    php7.2-mbstring php7.2-gd php7.2-zip
RUN apt-get install -y nginx supervisor curl
RUN apt-get install -y locales

RUN locale-gen de_CH.UTF-8
RUN locale-gen fr_CH.UTF-8
RUN locale-gen it_CH.UTF-8
RUN locale-gen en_GB.UTF-8

ENV LANG de_CH.UTF-8
ENV LANGUAGE de_CH.UTF-8
ENV LC_ALL de_CH.UTF-8

COPY nginx.conf /etc/nginx/nginx.conf
COPY nginx-defaults.conf /etc/nginx/defaults.conf
COPY htpasswd /etc/nginx/htpasswd
COPY fpm-pool.conf /etc/php/7.2/fpm/pool.d/www.conf
COPY php.ini /etc/php/7.2/fpm/conf.d/zzz_custom.ini

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN mkdir -p /app
RUN mkdir -p /app/public_html/

COPY phpinfo.php /app/public_html/index.php

WORKDIR /app

RUN mkdir -p /var/run/php

EXPOSE 80 81 82 83

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
